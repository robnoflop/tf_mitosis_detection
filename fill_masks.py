from Tkinter import *
from PIL import Image
from PIL import ImageTk
from random import *
import numpy as np
import os

import csv
import sys

def floodFill(x, y, a, b, c, image):
    toFill = set()
    toFill.add((x,y))
    i = a
    j = b
    k = c 
    while len(toFill) > 0:
        (x,y) = toFill.pop()
        if (x < image.width and y < image.height and x >= 0 and y >= 0):
            (r,g,b) = image.getpixel((x,y))
            if not (r,g,b) == (0,0,0):
                continue
            image.putpixel((x,y), (i,j,k))
            (r,g,b) = image.getpixel((x,y))
            toFill.add((x-1,y))
            toFill.add((x+1,y))
            toFill.add((x,y-1))
            toFill.add((x,y+1))
    return image

root_dir = 'Tensorflow-SegNet/dataset/test/lables'

for fn in os.listdir(root_dir):
    if "pngold.png" not in fn:
        if fn != "keep":
            image_path = root_dir + '/' + fn
            image = Image.open(image_path)
            if np.amax(image) > 0:
                print fn
                print np.amax(image)
                print np.amin(image)
        
    

    # if "pngold." not in fn:
    #     print(fn)
    #     image_path = root_dir + '/' + fn
    #     image = Image.open(image_path)
    #     if image.mode is not 'L':
    #         out = Image.new('L',(200,200))
    #         for x in range(0, image.height):
    #             for y in range(0, image.width):
    #                 a = image.getpixel((x,y))
    #                 if a != 40:
    #                     out.putpixel((x,y), (1))
    #                 #print(a)
    #         image.save(image_path+"old.png")
    #         image_path = root_dir + '/' + fn.split(".")[0] + ".mask.0.png"
    #         out.save(image_path)


    #image = image.convert('P')

    #image = floodFill(1, 1, 0, 255, 0, image)

    
