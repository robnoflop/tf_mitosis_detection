import os
import random
from PIL import Image
from PIL import ImageDraw

import csv
import sys

root_dir = '/media/robnoflop/OS/Users/RobNoFlop/Desktop/tf_mitosis_detection/raw_dataset/'
ground_truth_dir = root_dir + 'mitoses_ground_truth'
data_dir = root_dir + 'data'

number = 1932
extract_features = False

for root, dirs, filenames in os.walk(ground_truth_dir):
    for d in dirs:
        for fn in os.listdir(ground_truth_dir + '/' + d):
            image_path = data_dir + '/' + d + '/' + fn.split(".")[0] + '.tif'
            csv_path = ground_truth_dir + '/' + d + '/' + fn
            with open(csv_path, 'rb') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                image = Image.open(image_path)
                if extract_features:
                    for row in spamreader:
                        y = int(row[0]) - 150
                        x = int(row[1]) - 150
                        off = 100
                        out = image.crop((x - off, y - off, x + off, y + off))
                        out.save("../dataset_class0/" + str(number) + ".png")
                        draw = ImageDraw.Draw(out)
                        draw.rectangle([0, 0, 200, 200], (0, 0, 0), (0, 0, 0))
                        #draw.ellipse([60, 60, 140, 140], (255, 255, 255), (255, 255, 255))
                        out = out.convert('L')
                        out.save("../dataset_class0/lables/" + str(number) + ".png")
                        number += 1
                else:
                    for x in range(0, image.width, 175):
                        for y in range(0, image.height, 175):
                            off = 200
                            skip = False
                            for row in spamreader:
                                y_ = int(row[0])
                                x_ = int(row[1])

                                if y_ > y and y_ < y + off:
                                    skip = True
                                    break
                                if x_ > x and x_ < x + off:
                                    skip = True
                                    break

                            if x + off > image.width or y + off > image.height:
                                skip = True

                            if random.random() > 0.01:
                                skip = True

                            if not skip:
                                out = image.crop((x, y, x + off, y + off))
                                out.save("../dataset_class0/" + str(number) + ".png")
                                lable = Image.new("L", (200, 200))
                                lable.save("../dataset_class0/lables/" + str(number) + ".mask.0.png")
                                number += 1
            